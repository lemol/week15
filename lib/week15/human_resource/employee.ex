defmodule Week15.HumanResource.Employee do
  use Ecto.Schema
  import Ecto.Changeset

  alias Week15.Accounts.User

  schema "employees" do
    field :name, :string
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:name, :user_id])
    |> validate_required([:name])
  end
end
