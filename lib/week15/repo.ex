defmodule Week15.Repo do
  use Ecto.Repo,
    otp_app: :week15,
    adapter: Ecto.Adapters.Postgres
end
