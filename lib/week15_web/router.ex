defmodule Week15Web.Router do
  use Week15Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug Week15Web.Plug.AuthAccessPipeline
  end

  pipeline :graphql do
    plug Week15Web.Context
  end

  scope "/api" do
    pipe_through :api

    scope "/auth" do
      post "/identity/callback", Week15Web.AuthController, :identity_callback
    end

    forward "/graphiql", Absinthe.Plug.GraphiQL, schema: Week15Web.Schema

    pipe_through :authenticated
    pipe_through :graphql

    forward "/", Absinthe.Plug, schema: Week15Web.Schema
  end

  scope "/", Week15Web do
    pipe_through :browser

    get "/*path", RootController, :index
  end
end
