defmodule Week15Web.Schema.HumanResourceTypes do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Week15.Repo

  object :employee do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :user, :user, resolve: assoc(:user)
  end
end
