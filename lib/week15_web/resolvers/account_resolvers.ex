defmodule Week15Web.Resolvers.Accounts do
  def list_users(_parent, _args, _resolution) do
    {:ok, Week15.Accounts.list_users()}
  end
end
