defmodule Week15Web.Resolvers.HumanResource do
  alias Week15.Repo
  alias Week15.HumanResource.Employee

  def list_employees(_parent, _args, _resolution) do
    {:ok, Week15.HumanResource.list_employees()}
  end

  def get_current_employee(_parent, _args, %{context: %{current_user: current_user}}) do
    {:ok, Repo.get_by(Employee, user_id: current_user.id)}
  end
end
