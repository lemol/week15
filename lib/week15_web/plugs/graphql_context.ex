defmodule Week15Web.Context do
  @behaviour Plug

  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _) do
    context = build_context(conn)
    Absinthe.Plug.put_options(conn, context: context)
  end

  defp build_context(conn) do
    current_user = Week15.Guardian.Plug.current_resource(conn)
    %{current_user: current_user}
  end
end
