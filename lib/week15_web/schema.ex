defmodule Week15Web.Schema do
  use Absinthe.Schema
  import_types(Week15Web.Schema.AccountTypes)
  import_types(Week15Web.Schema.HumanResourceTypes)

  alias Week15Web.Resolvers

  query do
    @desc "Get authenticated employee"
    field :me, non_null(:employee) do
      resolve(&Resolvers.HumanResource.get_current_employee/3)
    end

    @desc "Get all users"
    field :users, non_null(list_of(non_null(:user))) do
      resolve(&Resolvers.Accounts.list_users/3)
    end

    @desc "Get all employees"
    field :employees, non_null(list_of(non_null(:employee))) do
      resolve(&Resolvers.HumanResource.list_employees/3)
    end
  end
end
