# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :week15,
  ecto_repos: [Week15.Repo]

# Configures the endpoint
config :week15, Week15Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "zyUES3X9MWOqiDcUh2ll7nvGWbUlMpLc1ljGrUVSTNBkbhecae+1jx3aIMYg7XNL",
  render_errors: [view: Week15Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Week15.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :ueberauth, Ueberauth,
  base_path: "/api/auth",
  providers: [
    identity:
      {Ueberauth.Strategy.Identity,
       [
         callback_methods: ["POST"],
         nickname_field: :username,
         param_nesting: "user",
         uid_field: :username
       ]}
  ]

config :week15, Week15.Guardian,
  issuer: "Week15",
  secret_key: "use mix phx.gen.secret",

  # We will get round to using these permissions at the end
  permissions: %{
    default: [:admin, :editor, :reader]
  }

# Configure the authentication plug pipeline
config :week15, Week15Web.Plug.AuthAccessPipeline,
  module: Week15.Guardian,
  error_handler: Week15Web.Plug.AuthErrorHandler
