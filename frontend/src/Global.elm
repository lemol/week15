module Global exposing (..)

import Browser.Events
import Data.Auth exposing (..)
import Element exposing (Device, classifyDevice)
import Json.Decode as D
import RemoteData
import UI.Modal as Modal



-- MODEL


type alias Model =
    { modal : Modal.Model
    , device : Device
    , auth : AuthState
    }


type alias Flags =
    { width : Int
    , height : Int
    }



-- MESSAGES


type Msg
    = NoOp
    | GotAccessToken (Maybe AccessToken)
    | GotAuthState AuthState
    | WindowResized Int Int
    | GotModalMsg Modal.Msg
    | RequestedAuth String String
    | CompletedAuth LoginWebData



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        GotAccessToken maybeToken ->
            case maybeToken of
                Just token ->
                    ( { model
                        | auth = Validating
                      }
                    , loadUser token CompletedAuth
                    )

                Nothing ->
                    update (GotAuthState NotAuthenticated) model

        GotAuthState auth ->
            ( { model | auth = auth }
            , Cmd.none
            )

        WindowResized width height ->
            ( { model | device = mkDevice width height }
            , Cmd.none
            )

        GotModalMsg subMsg ->
            let
                ( newModal, newCmd ) =
                    Modal.update subMsg model.modal
            in
            ( { model | modal = newModal }
            , Cmd.map GotModalMsg newCmd
            )

        RequestedAuth username password ->
            ( model
            , authenticate username password CompletedAuth
            )

        CompletedAuth data ->
            let
                auth =
                    case data of
                        RemoteData.Success ( token, user ) ->
                            Authenticated user token

                        RemoteData.Loading ->
                            Validating

                        RemoteData.NotAsked ->
                            IDLE

                        RemoteData.Failure _ ->
                            NotAuthenticated
            in
            ( { model | auth = auth }
            , storeAccessToken
                (auth
                    |> authStateToMaybe
                    |> Maybe.map Tuple.second
                    |> Maybe.withDefault ""
                    |> accessTokenEncoder
                )
            )


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        ( modalModel, modalCmd ) =
            Modal.init

        device =
            mkDevice flags.width flags.height
    in
    ( { modal = modalModel
      , device = device
      , auth = IDLE
      }
    , Cmd.batch
        [ requestAccessToken ()
        , Cmd.map GotModalMsg modalCmd
        ]
    )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ setAccessToken
            (D.decodeValue accessTokenDecoder
                >> Result.withDefault Nothing
                >> GotAccessToken
            )
        , Browser.Events.onResize WindowResized
        ]



-- UTILS


mkDevice : Int -> Int -> Device
mkDevice width height =
    classifyDevice
        { width = width
        , height = height
        }
