module Layout.Main exposing (LayoutMsg(..), Model, Msg(..), Page, init, refresh, splitMsg, update, view)

import Browser.Navigation as Navigation
import Data.Auth exposing (AuthState(..), requestLogout)
import Element exposing (..)
import Element.Events as Events
import Global
import UI exposing (Document)
import UI.SearchBox as SearchBox



-- DATA


type alias Page msg =
    { title : String
    , main : Element msg
    }



-- MODEL


type alias Model =
    { userMenuOpen : Bool
    , searchBox : SearchBox.State
    }



-- MESSAGE


type Msg
    = Logout
    | Login
    | SearchBoxMsg SearchBox.Msg


type LayoutMsg a
    = LayoutMsg Msg
    | PageMsg a


splitMsg : (Msg -> msg) -> (a -> msg) -> LayoutMsg a -> msg
splitMsg fromLayoutMsg fromPageMsg msg1 =
    case msg1 of
        LayoutMsg x ->
            fromLayoutMsg x

        PageMsg x ->
            fromPageMsg x



-- UPDATE


update : Msg -> Global.Model -> Model -> ( Model, Cmd Msg, Cmd Global.Msg )
update msg _ model =
    case msg of
        Logout ->
            ( model, Cmd.none, requestLogout () )

        Login ->
            ( model, Cmd.none, Navigation.load "/login" )

        SearchBoxMsg subMsg ->
            ( { model | searchBox = SearchBox.update subMsg model.searchBox }
            , Cmd.none
            , Cmd.none
            )


init : Global.Model -> ( Model, Cmd Msg )
init { auth } =
    let
        model =
            { userMenuOpen = False
            , searchBox = SearchBox.init
            }
    in
    ( model
    , refresh model auth
    )


refresh : Model -> AuthState -> Cmd Msg
refresh _ _ =
    Cmd.none



-- VIEWS


view : Global.Model -> Model -> Page msg -> Document (LayoutMsg msg)
view global model page =
    let
        title =
            page.title

        body =
            column
                [ height fill
                , width fill
                ]
                [ viewHeader global model
                    |> Element.map LayoutMsg
                , page.main
                    |> Element.map PageMsg
                ]
    in
    { title = title
    , body = body
    }


viewHeader : Global.Model -> Model -> Element Msg
viewHeader global model =
    let
        left =
            viewSearchBox model

        right =
            case global.auth of
                Authenticated user _ ->
                    column
                        []
                        [ text ("User: " ++ user.name)
                        , el [ Events.onClick Logout ]
                            (text "LOGOUT")
                        ]

                Validating ->
                    text "Validating"

                NotAuthenticated ->
                    column
                        []
                        [ text "NotAuthenticated"
                        , el [ Events.onClick Login ]
                            (text "LOGIN")
                        ]

                IDLE ->
                    text "IDLE"
    in
    row
        [ width fill ]
        [ left
        , right
        ]


viewSearchBox : Model -> Element Msg
viewSearchBox model =
    SearchBox.view
        [ width <| px 300
        , height <| px 28
        ]
        { placeholder = Just "Search users..."
        , state = model.searchBox
        , toMsg = SearchBoxMsg
        }
