module Pages.Login exposing (..)

import Element exposing (..)
import Element.Background as Bg
import Element.Border as Border
import Element.Input as Input exposing (labelAbove)
import Global
import UI exposing (Document)
import UI.Palette exposing (..)
import Utils exposing (send)


type alias Model =
    { username : String
    , password : String
    }


type Msg
    = ChangedUsername String
    | ChangedPassword String
    | SubmitedForm


init : ( Model, Cmd Msg )
init =
    ( { username = ""
      , password = ""
      }
    , Cmd.none
    )


update : Msg -> Global.Model -> Model -> ( Model, Cmd Msg, Cmd Global.Msg )
update msg _ model =
    case msg of
        ChangedUsername value ->
            ( { model | username = value }
            , Cmd.none
            , Cmd.none
            )

        ChangedPassword value ->
            ( { model | password = value }
            , Cmd.none
            , Cmd.none
            )

        SubmitedForm ->
            ( model
            , Cmd.none
            , send (Global.RequestedAuth model.username model.password)
            )


view : Global.Model -> Model -> Document Msg
view global model =
    { title = "Login"
    , body = viewBody global model
    }


viewBody : Global.Model -> Model -> Element Msg
viewBody global model =
    el
        [ Bg.color bgColor
        , width fill
        , height fill
        ]
        (viewLoginBox global model)


viewLoginBox : Global.Model -> Model -> Element Msg
viewLoginBox global model =
    column
        [ centerX
        , centerY
        , padding 12
        , Border.rounded 3
        , Bg.color white
        ]
        [ text "Login"
        , Input.text
            []
            { onChange = ChangedUsername
            , text = model.username
            , placeholder = Nothing
            , label = labelAbove [] (text "Utilizador")
            }
        , Input.text
            []
            { onChange = ChangedPassword
            , text = model.password
            , placeholder = Nothing
            , label = labelAbove [] (text "Palavra-passe")
            }
        , Input.button
            []
            { onPress = Just SubmitedForm
            , label = text "Entrar"
            }
        ]
