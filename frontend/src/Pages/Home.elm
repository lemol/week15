module Pages.Home exposing (..)

import Element exposing (..)
import Global
import Layout.Main as Layout
import UI exposing (Document)



-- MODEL


type alias Model =
    {}



-- MESSAGE


type Msg
    = NoOp



-- UPDATE


update : Msg -> Global.Model -> Model -> ( Model, Cmd Msg, Cmd Global.Msg )
update msg _ model =
    case msg of
        NoOp ->
            ( model
            , Cmd.none
            , Cmd.none
            )


init : Global.Model -> Maybe Layout.Model -> ( ( Model, Cmd Msg ), ( Layout.Model, Cmd Layout.Msg ) )
init global maybeLayout =
    let
        ( layout, layoutCmd ) =
            case maybeLayout of
                Just x ->
                    ( x, Cmd.none )

                Nothing ->
                    Layout.init global
    in
    ( ( {}
      , Cmd.none
      )
    , ( layout
      , layoutCmd
      )
    )



-- VIEW


view : Global.Model -> Layout.Model -> Model -> Document (Layout.LayoutMsg Msg)
view global layout model =
    Layout.view global layout (viewPage global model)


viewPage : Global.Model -> Model -> Layout.Page Msg
viewPage global model =
    { title = "Home page"
    , main = text "This is the main content"
    }
