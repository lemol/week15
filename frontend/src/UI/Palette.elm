module UI.Palette exposing (..)

import Element exposing (Color, rgb255)


bgColor : Color
bgColor =
    rgb255 0xF0 0xF0 0xF0


white : Color
white =
    rgb255 0xFF 0xFF 0xFF
