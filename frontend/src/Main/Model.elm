module Main.Model exposing (Flags, Model)

import Browser.Navigation as Navigation
import Global
import Page



-- MODEL


type alias Model =
    { page : Page.Model
    , global : Global.Model
    , key : Navigation.Key
    }


type alias Flags =
    Global.Flags
