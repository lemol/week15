module Main.Messages exposing (Msg(..))

import Browser
import Global
import Page
import Url



-- MESSAGE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | PageMsg Page.Msg
    | GlobalMsg Global.Msg
