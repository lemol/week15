import { Elm } from "../Main";

const app = Elm.Main.init({
  node: document.getElementById("app"),
  flags: {
    width: window.innerWidth,
    height: window.innerHeight
  }
});

app.ports.requestAccessToken.subscribe(async () => {
  const token = localStorage.getItem("accessToken");

  if (!token) {
    app.ports.setAccessToken.send(null);
    return;
  }

  app.ports.setAccessToken.send(token);
});

app.ports.storeAccessToken.subscribe(async (token: string) => {
  localStorage.setItem("accessToken", token);
});

app.ports.requestLogout.subscribe(async () => {
  localStorage.removeItem("accessToken");

  app.ports.setAccessToken.send(null);
});
