module Page exposing (Model, Msg(..), enterRoute, init, onSetAuth, update, view)

import Data.Auth exposing (AuthState(..))
import Element exposing (..)
import Global
import Layout.Main as MainLayout
import Pages.Home as Home
import Pages.Login as Login
import Routing exposing (Route(..))
import UI exposing (Document, mapDocument)



-- MODEL


type alias Model =
    { route : Route
    , mainLayout : Maybe MainLayout.Model
    , loginPage : Maybe Login.Model
    , homePage : Maybe Home.Model
    }


init : () -> Global.Model -> Route -> ( Model, Cmd Msg, Cmd Global.Msg )
init _ global route =
    let
        model =
            { route = route
            , mainLayout = Nothing
            , loginPage = Nothing
            , homePage = Nothing
            }
    in
    enterRoute global route model



-- MESSAGE


type Msg
    = MainLayoutMsg MainLayout.Msg
    | LoginMsg Login.Msg
    | HomeMsg Home.Msg



-- UPDATE


update : Msg -> Global.Model -> Model -> ( Model, Cmd Msg, Cmd Global.Msg )
update msg global model =
    case msg of
        MainLayoutMsg subMsg ->
            let
                ( pageModel, pageCmd, globalCmd ) =
                    maybeUpdate MainLayout.update subMsg global model.mainLayout
            in
            ( { model | mainLayout = pageModel }
            , Cmd.map MainLayoutMsg pageCmd
            , globalCmd
            )

        LoginMsg subMsg ->
            let
                ( pageModel, pageCmd, globalCmd ) =
                    maybeUpdate Login.update subMsg global model.loginPage
            in
            ( { model | loginPage = pageModel }
            , Cmd.map LoginMsg pageCmd
            , globalCmd
            )

        HomeMsg subMsg ->
            let
                ( pageModel, pageCmd, globalCmd ) =
                    maybeUpdate Home.update subMsg global model.homePage
            in
            ( { model | homePage = pageModel }
            , Cmd.map HomeMsg pageCmd
            , globalCmd
            )


onSetAuth : Global.Model -> Model -> AuthState -> Cmd Msg
onSetAuth global model auth =
    Cmd.batch
        []



-- VIEW


view : Global.Model -> Model -> Document Msg
view global model =
    case model.route of
        NotFoundRoute ->
            { title = "404", body = Element.text "Not Found" }

        LoginRoute ->
            model.loginPage
                |> Maybe.map (Login.view global)
                |> Maybe.withDefault viewEmpty
                |> mapDocument LoginMsg

        HomeRoute ->
            Maybe.map2 (Home.view global) model.mainLayout model.homePage
                |> Maybe.withDefault viewEmpty
                |> mapDocument (MainLayout.splitMsg MainLayoutMsg HomeMsg)


viewEmpty : Document msg
viewEmpty =
    { title = "Week15"
    , body = Element.none
    }



-- ROUTING


enterRoute : Global.Model -> Route -> Model -> ( Model, Cmd Msg, Cmd Global.Msg )
enterRoute global route model =
    let
        ( newModel, cmd ) =
            case route of
                NotFoundRoute ->
                    ( model, Cmd.none )

                LoginRoute ->
                    let
                        ( pageModel, pageCmd ) =
                            Login.init
                    in
                    ( { model | loginPage = Just pageModel }
                    , Cmd.map LoginMsg pageCmd
                    )

                HomeRoute ->
                    let
                        ( pageInit, layoutInit ) =
                            Home.init global model.mainLayout
                    in
                    ( { model
                        | homePage = Tuple.first pageInit |> Just
                        , mainLayout = Tuple.first layoutInit |> Just
                      }
                    , Cmd.batch
                        [ Cmd.map HomeMsg (Tuple.second pageInit)
                        , Cmd.map MainLayoutMsg (Tuple.second layoutInit)
                        ]
                    )
    in
    ( { newModel
        | route = route
      }
    , cmd
    , Cmd.none
    )



-- UTILS


maybeUpdate : (msg -> Global.Model -> model -> ( model, Cmd msg, Cmd Global.Msg )) -> msg -> Global.Model -> Maybe model -> ( Maybe model, Cmd msg, Cmd Global.Msg )
maybeUpdate realUpdate msg global maybeModel =
    case maybeModel of
        Just model ->
            realUpdate msg global model
                |> mapFirst Just

        Nothing ->
            ( Nothing, Cmd.none, Cmd.none )


mapFirst : (a -> d) -> ( a, b, c ) -> ( d, b, c )
mapFirst f ( a, b, c ) =
    ( f a, b, c )
