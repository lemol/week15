port module Data.Auth exposing (AccessToken, AuthState(..), LoginWebData, RemoteError, User, accessTokenDecoder, accessTokenEncoder, authStateToMaybe, authenticate, loadUser, requestAccessToken, requestLogout, setAccessToken, storeAccessToken)

import Api.Object
import Api.Object.Employee
import Api.Object.User
import Api.Query
import Graphql.Http
import Graphql.Operation exposing (RootQuery)
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet)
import Http exposing (Response(..))
import Json.Decode as Decode
import Json.Encode as Encode
import RemoteData exposing (RemoteData)
import Task exposing (Task)



-- PORTS


port requestAccessToken : () -> Cmd msg


port requestLogout : () -> Cmd msg


port storeAccessToken : Encode.Value -> Cmd msg


port setAccessToken : (Encode.Value -> msg) -> Sub msg



-- DATA


type AuthState
    = IDLE
    | Validating
    | NotAuthenticated
    | Authenticated User AccessToken


type alias User =
    { name : String
    , username : String
    }


type alias AccessToken =
    String


type RemoteError
    = RestError Http.Error
    | GraphqlError (Graphql.Http.Error ())


type alias LoginWebData =
    RemoteData RemoteError ( AccessToken, User )



-- UTILS


authStateToMaybe : AuthState -> Maybe ( User, AccessToken )
authStateToMaybe auth =
    case auth of
        Authenticated user token ->
            Just ( user, token )

        _ ->
            Nothing



-- SERIALIZATION


authDecoder : Decode.Decoder AccessToken
authDecoder =
    Decode.field
        "data"
        (Decode.field "token" Decode.string)


userSelection : SelectionSet User Api.Object.Employee
userSelection =
    SelectionSet.map2 User
        Api.Object.Employee.name
        (SelectionSet.map
            (Maybe.withDefault "")
            (Api.Object.Employee.user Api.Object.User.username)
        )


accessTokenEncoder : AccessToken -> Encode.Value
accessTokenEncoder =
    Encode.string


accessTokenDecoder : Decode.Decoder (Maybe AccessToken)
accessTokenDecoder =
    Decode.nullable Decode.string



-- API


authenticate : String -> String -> (LoginWebData -> msg) -> Cmd msg
authenticate username password toMsg =
    let
        getTokenTask =
            createToken username password
                |> mapTaskHttpError

        getUserTask token =
            getUser token
                |> Task.map (\user -> ( token, user ))
                |> mapTaskGraphqlError
    in
    getTokenTask
        |> Task.andThen getUserTask
        |> RemoteData.fromTask
        |> Task.perform toMsg


loadUser : AccessToken -> (LoginWebData -> msg) -> Cmd msg
loadUser token toMsg =
    let
        getUserTask =
            getUser token
                |> Task.map (\user -> ( token, user ))
                |> mapTaskGraphqlError
    in
    getUserTask
        |> RemoteData.fromTask
        |> Task.perform toMsg


createToken : String -> String -> Task Http.Error AccessToken
createToken username password =
    Http.task
        { method = "POST"
        , headers = []
        , url = "/api/auth/identity/callback"
        , body =
            Http.jsonBody <|
                Encode.object
                    [ ( "user"
                      , Encode.object
                            [ ( "username", Encode.string username )
                            , ( "password", Encode.string password )
                            ]
                      )
                    ]
        , resolver =
            Http.stringResolver (handleJsonResponse authDecoder)
        , timeout = Nothing
        }


getUser : AccessToken -> Task (Graphql.Http.Error ()) User
getUser token =
    let
        query : SelectionSet User RootQuery
        query =
            Api.Query.me userSelection
    in
    query
        |> Graphql.Http.queryRequest "/api"
        |> Graphql.Http.withHeader "authorization" ("Bearer " ++ token)
        |> Graphql.Http.toTask
        |> Task.mapError (Graphql.Http.mapError (always ()))



-- UTILS


mapTaskHttpError : Task Http.Error a -> Task RemoteError a
mapTaskHttpError =
    Task.mapError RestError


mapTaskGraphqlError : Task (Graphql.Http.Error ()) a -> Task RemoteError a
mapTaskGraphqlError =
    Task.mapError GraphqlError


handleJsonResponse : Decode.Decoder a -> Http.Response String -> Result Http.Error a
handleJsonResponse decoder response =
    case response of
        Http.BadUrl_ url ->
            Err (Http.BadUrl url)

        Http.Timeout_ ->
            Err Http.Timeout

        Http.BadStatus_ { statusCode } _ ->
            Err (Http.BadStatus statusCode)

        Http.NetworkError_ ->
            Err Http.NetworkError

        Http.GoodStatus_ _ body ->
            case Decode.decodeString decoder body of
                Err _ ->
                    Err (Http.BadBody body)

                Ok result ->
                    Ok result
