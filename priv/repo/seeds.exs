# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Week15.Repo.insert!(%Week15.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Week15.Repo
alias Week15.HumanResource.Employee

with {:ok, _user} <-
       Week15.Accounts.create_user(%{
         username: "lemol",
         password: "lemol.key",
         roles: %{default: [:admin]}
       }) do
end

with {:ok, _user} <-
       Week15.Accounts.create_user(%{
         username: "frida",
         password: "frida.key",
         roles: %{default: [:editor]}
       }) do
end

with {:ok, _user} <-
       Week15.Accounts.create_user(%{
         username: "mercy",
         password: "mercy.key",
         roles: %{default: [:reader]}
       }) do
end

with nil <- Repo.get_by(Employee, user_id: 1) do
  {:ok, _user} =
    Week15.HumanResource.create_employee(%{
      name: "Leza Lutonda",
      user_id: 1
    })
end

with nil <- Repo.get_by(Employee, user_id: 2) do
  {:ok, _user} =
    Week15.HumanResource.create_employee(%{
      name: "Frida Zohounvo",
      user_id: 2
    })
end
